package com.sitric;

/*
     @SpringBootApplication включает сканирование компонентов,
     автоконфигурацию и показывает разным компонентам Spring (например, интеграционным тестам),
     что это Spring Boot приложение.
*/
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception{
        SpringApplication.run(Application.class, args);
    }

    /*
         SpringApplication.run() - хелпер, который используя список предоставленных конфигураций
         (класс Application сам по себе конфигурация) создает ApplicationContext, конфигурирует его,
         выводит баннер в консоли и засекает время старта приложения и т.п.
    */
}
