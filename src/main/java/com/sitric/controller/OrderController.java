package com.sitric.controller;
/*
 REST controller
 */

import com.sitric.model.Order;
import com.sitric.model.Product;
import com.sitric.model.ShoppingCart;
import com.sitric.repository.OrderRepository;
import com.sitric.repository.ShoppingCartRepository;
import com.sitric.service.OrderAutoGeneration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class OrderController{
    private static final Logger logger = Logger.getLogger(OrderController.class);

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ShoppingCartRepository cartRepository;

    @Autowired
    OrderAutoGeneration orderAutoGeneration;

    public OrderController() {
        orderAutoGeneration = new OrderAutoGeneration();
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Order> main(
            @RequestParam(value="command") String command,
            @RequestParam(value="order_id",required = false) String orderId,
            @RequestParam(value="cart_id",required = false) String cartId
    ){
        List<Order> ordersToShow = new LinkedList<>();
        if (command.equals("readAll")) {
            logger.debug("All orders are shown");
            ordersToShow = orderRepository.findAll();
        }
        else if (command.equals("readById")){
            if (orderId != null) {
                ordersToShow.add(orderRepository.findById(UUID.fromString(orderId)).get());
            }

            if (ordersToShow.isEmpty()) {
                logger.debug("Order with id: " + orderId + " not found");
            }
            else {
                logger.debug("Order with id: " + orderId + " loaded successfully");
            }
        }
        else if (command.equals("create")){
            for (int i = 0; i < 5; i++) {
                orderRepository.save(orderAutoGeneration.generate());
            }
            logger.debug("Orders created successfully");

        }
        return ordersToShow;
    }

    @RequestMapping(method=RequestMethod.POST)
    public UUID addToCart(
            @RequestParam(value="command") String command,
            @RequestParam(value="cart_id",required = false) String cartId
    ){
        UUID id = null;
        if (command.equals("addToCart") && cartId != null) {
            Optional<ShoppingCart> optional = cartRepository.findById(UUID.fromString(cartId));
            if (optional.isPresent()) {
                ShoppingCart tempCart = optional.get();
                Product product = orderAutoGeneration.generateProduct();
                id = product.getId();
                tempCart.add(product);
                cartRepository.save(tempCart);
                logger.debug("Product with id: " + id + " added to cart (cart id: " + cartId + ")");
                logger.debug("Orders structure updated successfully");
            }
            else {
                logger.debug("Cart with id: " + cartId + " not found");
            }

        }
        return id;
    }

    @RequestMapping(method=RequestMethod.DELETE)
    public int removeOrder(
            @RequestParam(value="command") String command,
            @RequestParam(value="order_id",required = false) String orderId
    ){
        int response = -1;
        try {
            if (command.equals("delById") && orderId != null) {
                orderRepository.deleteById(UUID.fromString(orderId));
                response = 0;                                               /*response=0*/
            }
        } catch (EmptyResultDataAccessException ex) {
            return 1;                                                       /*response=1*/
//               ex.getMessage();
        }
        return response;
    }
}
