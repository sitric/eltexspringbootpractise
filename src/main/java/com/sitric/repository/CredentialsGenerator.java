package com.sitric.repository;

import com.sitric.model.Credentials;

public interface CredentialsGenerator {
    Credentials create();
}
