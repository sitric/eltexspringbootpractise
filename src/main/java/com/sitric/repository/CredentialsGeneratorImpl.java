package com.sitric.repository;

import com.sitric.model.Credentials;


import java.util.ArrayList;
import java.util.List;

public class CredentialsGeneratorImpl implements CredentialsGenerator {
    @Override
    public Credentials create() {
        List<Credentials> credentials = new ArrayList<>();
        credentials.add(new Credentials("Michael", "Caldwell", "C.", "masl@mail.ru"));
        credentials.add(new Credentials("David", "People", "S.", "kar@mail.ru"));
        credentials.add(new Credentials("Stephen", "Hines", "E.", "voyei@mail.ru"));

        return credentials.get((int)(Math.random()*credentials.size()));
    }
}
