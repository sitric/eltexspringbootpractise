package com.sitric.repository;

import com.sitric.model.Tea;

public class TeaGeneratorImpl implements TeaGenerator {
    public Tea create() {
        String[] titles = {"Lipton", "Grienfield", "Princess Nouri", "Puer", "Tie Guan Yin"};
        String[] producer = {"Lipton ltd", "Famous Tea Company", "Multon"};
        double[] price = {100.00, 150.00, 200.00, 250.00, 300.00};
        String[] country = {"Russia", "India", "China"};
        String[] pack = {"cardboard", "wood"};

        return new Tea(titles[(int)(Math.random() * (titles.length))],
                price[(int)(Math.random() * (price.length))],
                producer[(int)(Math.random() * (producer.length))],
                country[(int)(Math.random() * (country.length))],
                pack[(int)(Math.random() * (pack.length))]);
    }
}
