package com.sitric.repository;

import com.sitric.model.Coffee;

public class CoffeeGeneratorImpl implements CoffeeGenerator {

    @Override
    public Coffee create() {
        String[] titles = {"Nescafe", "McCoffee", "Pele", "Cart Noire"};
        String[] producer = {"Lipton ltd", "Famous Tea Company", "Multon", "Nescafe Corp"};
        double[] price = {100.00, 150.00, 200.00, 250.00, 300.00};
        String[] country = {"Russia", "India", "China"};
        String[] bean_type = {"Arabica", "Robusta"};

        return new Coffee(titles[(int)(Math.random() * (titles.length))],
                price[(int)(Math.random() * (price.length))],
                producer[(int)(Math.random() * (producer.length))],
                country[(int)(Math.random() * (country.length))],
                bean_type[(int)(Math.random() * (bean_type.length))]);
    }
}
