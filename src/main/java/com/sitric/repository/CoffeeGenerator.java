package com.sitric.repository;

import com.sitric.model.Coffee;

public interface CoffeeGenerator {
    Coffee create();
}
