package com.sitric.repository;

import com.sitric.model.Tea;

public interface TeaGenerator {
    Tea create();
}
