package com.sitric.model;

/*
    класс "Заказ"
*/

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.sitric.model.Credentials;
import com.sitric.model.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

@JsonAutoDetect
@Entity
@Table(name = "user_order")
public class Order implements Serializable {
    @Id
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "order_status", nullable = false)
    private String orderStatus; //в ожидании, обработан

    @Column(name = "creation_time", nullable = false)
    private long creationTime;

    @Column(name = "waiting_time", nullable = false)
    private long waitingTime; // время, через которое заказ должен исчезнуть


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", nullable = false)
    private ShoppingCart shoppingCart;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "credentials_id", nullable = false)
    private Credentials credentials;

    public Order() {
    }

    public Order(ShoppingCart cart, Credentials credentials) {
        this.id = UUID.randomUUID();
        this.orderStatus = "in process";
        this.creationTime = Calendar.getInstance().getTimeInMillis();
        this.waitingTime = (long)(Math.random() * 1000);
        this.shoppingCart = cart;
        this.credentials = credentials;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(long waitingTime) {
        this.waitingTime = waitingTime;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }



    @Override
    public String toString() {
        return "Order{" +
                "orderStatus='" + orderStatus + '\'' +
                ", creation time='" + creationTime + '\'' +
                ", waiting time='" + waitingTime + '\'' +
                ", id=" + id +
                ", shoppingCart=" + shoppingCart +
                ", credentials=" + credentials +
                '}';
    }
}
