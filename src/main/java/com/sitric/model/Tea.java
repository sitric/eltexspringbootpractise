package com.sitric.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tea")
public class Tea extends Product {

    @Column(name = "pack")
    private String pack;

    public Tea() {
    }

    public Tea(String title, double price, String producer, String country, String pack) {
        super(title, price, producer, country);
        this.pack = pack;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    @Override
    public String toString() {
        return "Tea{" + super.toString() +
                "pack='" + pack + '\'' +
                '}';
    }
}
