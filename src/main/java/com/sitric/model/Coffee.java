package com.sitric.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "coffee")
public class Coffee extends Product {

    @Column(name = "bean_type")
    private String beanType;

    public Coffee() {}

    public Coffee(String title, double price, String producer, String country, String beanType) {
        super(title, price, producer, country);
        this.beanType = beanType;
    }

    public String getBeanType() {
        return beanType;
    }

    public void setBeanType(String beanType) {
        this.beanType = beanType;
    }

    @Override
    public String toString() {
        return "Coffee{" + super.toString() +
                "beanType='" + beanType + '\'' +
                '}';
    }
}
