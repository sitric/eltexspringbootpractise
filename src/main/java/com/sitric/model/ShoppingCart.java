package com.sitric.model;


import org.springframework.stereotype.Component;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*
    класс-коллекция "корзина"
    считается, что корзина может сущестовать без связи с заказом
*/

@Entity
@Table(name = "shopping_cart")
@Component
public class ShoppingCart implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID Id;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Product> products;

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        Id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public ShoppingCart() {
        this.products = new ArrayList<>();
        this.Id = UUID.randomUUID();
    }

    public void add(Product product){
        products.add(product);
    }

    public void delete(Product product){
        products.remove(product);
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "Id=" + Id +
                ", products=" + products +
                '}';
    }
}
