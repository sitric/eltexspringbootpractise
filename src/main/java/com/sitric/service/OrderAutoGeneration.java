package com.sitric.service;

import com.sitric.model.Order;
import com.sitric.model.ShoppingCart;
import com.sitric.model.Product;
import com.sitric.repository.CoffeeGeneratorImpl;
import com.sitric.repository.CredentialsGeneratorImpl;
import com.sitric.repository.TeaGeneratorImpl;
import org.springframework.stereotype.Service;


/**/
@Service
public class OrderAutoGeneration{

    private TeaGeneratorImpl teaGenerator;
    private CoffeeGeneratorImpl coffeeGenerator;
    private CredentialsGeneratorImpl credentialsGenerator;

    public OrderAutoGeneration() {
        teaGenerator = new TeaGeneratorImpl();
        coffeeGenerator = new CoffeeGeneratorImpl();
        credentialsGenerator = new CredentialsGeneratorImpl();
    }

    public Product generateProduct(){
        return Math.random() > 0.5 ? teaGenerator.create(): coffeeGenerator.create();
    }

    private ShoppingCart generateCart(){
        ShoppingCart cart = new ShoppingCart();
        int productCount = (int)(Math.random() * 7 + 1);
        for (int i = 0; i < productCount; i++) {
            cart.add(generateProduct());
        }
        return cart;
    }

    public Order generate(){
        return new Order(generateCart(), credentialsGenerator.create());
    }
}
