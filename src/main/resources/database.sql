CREATE SCHEMA IF NOT EXISTS ordermanager_db
  DEFAULT CHARACTER SET utf8;
USE ordermanager_db;


-- Insert data:

-- tea
INSERT INTO tea VALUES ('ffa4d46d-677e-485c-97b6-e889929333e4', 'Принцесса Нури', 200.00, 'ЗАО Мултон', 'Индия', 'жесть');
INSERT INTO tea VALUES ('c40dbc89-7b8a-4cd4-b4df-86bdbe5b7f08', 'Принцесса Нури', 200.00, 'ЗАО Мултон', 'Россия', 'жесть');
INSERT INTO tea VALUES ('6c0b61dc-bb30-41f0-920c-9c0a955f7794', 'Тигуань-инь', 300.00, 'Lipton ltd', 'Индия', 'картон');
INSERT INTO tea VALUES ('923be820-c145-440f-ab67-f33eb4f11e86', 'Пуэр', 300.00, 'Famous Tea Company', 'Китай', 'картон');

-- coffee
INSERT INTO coffee VALUES ('cfe6a003-c254-4aee-ab40-8bedd2edada9', 'Nescafe', 250.00, 'Lipton ltd', 'Индия', 'робуста');
INSERT INTO coffee VALUES ('d74ebf85-e2e3-4881-a83a-798f6d6a6abb', 'Cart Noire', 100.00, 'Nescafe Corp', 'Индия', 'робуста');
INSERT INTO coffee VALUES ('2db6d50f-33d7-42c8-8c40-c74b67daaad1', 'Nescafe', 150.00, 'Nescafe Corp', 'Индия', 'робуста');
INSERT INTO coffee VALUES ('ccfad2d1-3043-4bb9-9613-49b4966f2a2d', 'Nescafe', 300.00, 'ЗАО Мултон', 'Китай', 'арабика');
INSERT INTO coffee VALUES ('5953225f-d7ba-46e4-88f0-d80c9503d46a', 'Пеле', 150.00, 'Lipton ltd', 'Россия', 'робуста');
INSERT INTO coffee VALUES ('efd94848-7469-4acc-8310-bbd7fed6a113', 'McCoffee', 300.00, 'Nescafe Corp', 'Индия', 'робуста');

-- credentials
INSERT INTO credentials VALUES ('38f1dd22-0dad-4c7e-9a53-8d5a3c62e13a', 'Коржубаев', 'Кир', 'Даниилович', 'kar@mail.ru');
INSERT INTO credentials VALUES ('e80a7f04-f8bd-4dcb-a80e-d48b3606f935', 'Маслюка', 'Ефросинья', 'Серафимовна', 'masl@mail.ru');
INSERT INTO credentials VALUES ('5107aaac-802e-4bdc-895e-541f472e3c8a', 'Цирульников', 'Самсон', 'Никанорович', 'tscir@mail.ru');

-- shopping_cart
INSERT INTO shopping_cart VALUES ('13cd3739-2841-4a3e-8757-ce59c7f1c823');
INSERT INTO shopping_cart VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097');
INSERT INTO shopping_cart VALUES ('84eade9e-41ef-4c70-8c70-a459063adcfa');

-- cart_products
INSERT INTO cart_products VALUES ('13cd3739-2841-4a3e-8757-ce59c7f1c823', 'ffa4d46d-677e-485c-97b6-e889929333e4');
INSERT INTO cart_products VALUES ('13cd3739-2841-4a3e-8757-ce59c7f1c823', 'cfe6a003-c254-4aee-ab40-8bedd2edada9');
INSERT INTO cart_products VALUES ('13cd3739-2841-4a3e-8757-ce59c7f1c823', 'd74ebf85-e2e3-4881-a83a-798f6d6a6abb');
-- *********************************************
INSERT INTO cart_products VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097', '2db6d50f-33d7-42c8-8c40-c74b67daaad1');
INSERT INTO cart_products VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097', 'ccfad2d1-3043-4bb9-9613-49b4966f2a2d');
INSERT INTO cart_products VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097', '5953225f-d7ba-46e4-88f0-d80c9503d46a');
INSERT INTO cart_products VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097', 'efd94848-7469-4acc-8310-bbd7fed6a113');
INSERT INTO cart_products VALUES ('fac472b4-4f25-4da6-9833-b4d1d0722097', '6c0b61dc-bb30-41f0-920c-9c0a955f7794');
-- *********************************************
INSERT INTO cart_products VALUES ('84eade9e-41ef-4c70-8c70-a459063adcfa', '6c0b61dc-bb30-41f0-920c-9c0a955f7794');
INSERT INTO cart_products VALUES ('84eade9e-41ef-4c70-8c70-a459063adcfa', '923be820-c145-440f-ab67-f33eb4f11e86');
INSERT INTO cart_products VALUES ('84eade9e-41ef-4c70-8c70-a459063adcfa', '5953225f-d7ba-46e4-88f0-d80c9503d46a');
INSERT INTO cart_products VALUES ('84eade9e-41ef-4c70-8c70-a459063adcfa', 'cfe6a003-c254-4aee-ab40-8bedd2edada9');
-- *********************************************

-- order
INSERT INTO user_order VALUES ('8b2f68ad-bcb7-4041-b260-8fbb507c7715', 1532503255686, 'waiting', 250, '38f1dd22-0dad-4c7e-9a53-8d5a3c62e13a', '13cd3739-2841-4a3e-8757-ce59c7f1c823');
INSERT INTO user_order VALUES ('13cd3739-2841-4a3e-8757-ce59c7f1c823', 1532503256004, 'waiting', 270, 'e80a7f04-f8bd-4dcb-a80e-d48b3606f935', 'fac472b4-4f25-4da6-9833-b4d1d0722097');
INSERT INTO user_order VALUES ('99a7fb23-5caa-4c47-8629-7606b1de9578', 1532503256305, 'waiting', 374, '5107aaac-802e-4bdc-895e-541f472e3c8a', '84eade9e-41ef-4c70-8c70-a459063adcfa');