## Summary

This repository is the result of the laboratory work #8 of the summer programming school Eltex.
This work relies on the subject area of ​​the 1 version of the Terms of Reference [link](http://www.c-java.ru/wp-content/uploads/2017/09/Java_JavaEE.txt)
and is a partial continuation of laboratory works 1-6, the code of which is in the repository [EltexJavaPractise](https://bitbucket.org/sitric/eltexjavapractise/src/master/)
The purpose of the laboratory work is acquaintance with the methodology of programming Java Enterprise Edition, the study of the technology stack: Maven, Spring Boot, Hibernate, MySQL.
Also familiarity with the methodology of REST.

The application does not use the view layer, the database structure is a clear adherence to the specifications of the technical assignment and is not normalized.
 
## О репозитории

Данный репозиторий является результатом выполнения лабораторной работы №8 летней школы программирования "Элтекс". 
Данная работа опирается на предметную область 1 варианта технического задания [ссылка](http://www.c-java.ru/wp-content/uploads/2017/09/Java_JavaEE.txt) 
и является частичным продолжением лабораторных работ 1-6, код которых находится в репозитории [EltexJavaPractise](https://bitbucket.org/sitric/eltexjavapractise/src/master/)
Целью лабораторной работы является знакомство с методологией программирования Java Enterprise Edition, изучение стека технологий: Maven, Spring Boot, Hibernate, MySQL.
Также знакомство с методологией REST. 

Приложение не использует слой view, структура базы данных является четким следованием указаний технического задания и не является нормализованной. 


